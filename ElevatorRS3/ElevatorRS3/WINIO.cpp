#include "WINIO.h"
#include <Windows.h>

HANDLE OpenDriver() {
	return CreateFileA(DEVICENAME, GENERIC_READ | GENERIC_WRITE | FILE_GENERIC_EXECUTE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_TEMPORARY, 0);
}

uint64_t* DriverMapMemory(HANDLE hDevice, uint64_t physAddr, size_t size, winioMem& mem) {
	if (INVALID_HANDLE_VALUE != hDevice)
	{
		memset(&mem, 0, sizeof(winioMem));
		mem.offset = physAddr;
		mem.size = size;
		DWORD retSize;
		BOOL r = DeviceIoControl(hDevice, IOCTL_MAPMEMORY, &mem, sizeof(winioMem), &mem, sizeof(winioMem), &retSize, 0);
		if (r)
		{
			return (uint64_t*)mem.outPtr;
		}
	}
	return nullptr;
}
BYTE DriverUnmapMemory(HANDLE hDevice, winioMem& mem) {
	if (INVALID_HANDLE_VALUE != hDevice)
	{
		DWORD retSize;
		return DeviceIoControl(hDevice, IOCTL_UNMAPMEM, &mem, sizeof(winioMem), 0, 0, &retSize, 0);
	}
	else
	{
		//printf("[INT_ERROR] INVALID_HANDLE_VALUE in DriverUnmapMemory\n");
		return 0;
	}
}

bool CloseDriver(HANDLE hDriver) {
	return CloseHandle(hDriver);
}