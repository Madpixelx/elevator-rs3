#pragma once

#include <stdint.h>
#include <windows.h>
#include <winternl.h>

#pragma pack(push)
#pragma pack(1)
struct winioMem
{
	uint64_t size; //read as LARGE_INTEGER
	uint64_t offset; //offset
	uint64_t unk1;
	uint64_t outPtr; // Where memory is mapped;
	uint64_t unk2;
};
#pragma pack(pop)

//#define IOCTL_MAPMEMORY 0x9C402580
#define IOCTL_MAPMEMORY 0x80102040
//#define IOCTL_UNMAPMEM 0x9C402584
#define IOCTL_UNMAPMEM 0x80102044

#define DEVICENAME "\\\\.\\WINIO"
//#define DEVICENAME "\\\\.\\ASMMAP64"



HANDLE OpenDriver();

uint64_t* DriverMapMemory(HANDLE, uint64_t physAddr, size_t size, winioMem& mem);
BYTE DriverUnmapMemory(HANDLE hDevice, winioMem& mem);

//bool DriverMapMemory(HANDLE, IoCommand*);
//bool DriverUnmapMemory(HANDLE, IoCommand*);
bool CloseDriver(HANDLE);