#pragma once
#define UNLOAD 16732

class StuffManager {
public:
	StuffManager()
	{
		hDll = LoadLibraryW(TEXT("c:\\windows\\system32\\ntdll.dll"));
		RtlInitUnicodeString(&usDriverName, L"\\Registry\\Machine\\System\\CurrentControlSet\\Services\\iowin");
		RtlInitUnicodeString(&fakeDriverName, L"\\Registry\\Machine\\System\\CurrentControlSet\\Services\\hwpolicy");
		RtlInitUnicodeString(&fakeDriverNameOnce, L"\\Registry\\Machine\\System\\CurrentControlSet\\Services\\WdFilter");
	}
	~StuffManager() {
		FreeLibrary(hDll);
	}

	DWORD FindPID(const std::wstring& processName) {
		PROCESSENTRY32 processInfo;
		processInfo.dwSize = sizeof(processInfo);

		HANDLE processesSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		if (processesSnapshot == INVALID_HANDLE_VALUE)
			return 0;

		Process32First(processesSnapshot, &processInfo);
		if (!processName.compare(processInfo.szExeFile))
		{
			CloseHandle(processesSnapshot);
			return processInfo.th32ProcessID;
		}

		while (Process32Next(processesSnapshot, &processInfo))
		{
			if (!processName.compare(processInfo.szExeFile))
			{
				CloseHandle(processesSnapshot);
				return processInfo.th32ProcessID;
			}
		}

		CloseHandle(processesSnapshot);
		return 0;
	}
	/*
	* Copies or deletes driver file
	*/
	BYTE CopyDeleteDriverFile(USHORT copy)
	{
		wchar_t local_path[MAX_PATH];
		GetCurrentDirectory(MAX_PATH, local_path);
		std::wstring filename = local_path;
		filename += L"\\WinIo64.sys";
		if (copy)
			CopyFileW(filename.c_str(), L"C:\\Windows\\WinIo64.sys", false);
		else
			DeleteFileW(L"C:\\Windows\\WinIo64.sys");
		return 0;
	}
	/*
	*Load with start of the program, unload when memory is mapped
	*/
	BYTE LoadDriver(USHORT type) 
	{
		
		if (!hDll)
			return true;
		if (type == UNLOAD)
		{
			*(FARPROC *)&ZwUnloadDriver = GetProcAddress(hDll, "ZwUnloadDriver");
			if (!ZwUnloadDriver)
				return true;

			NTSTATUS ntStatus = ZwUnloadDriver(&usDriverName);
			if (FAILED(ntStatus)){
				printf("ERROR UNLOADING: %x \n", ntStatus);
				return true;
			}
			else
				printf("Driver unloaded successfully!\n");
			//delete reg key
			RegOpenKeyExW(HKEY_LOCAL_MACHINE, L"System\\CurrentControlSet\\Services",
				0, KEY_CREATE_SUB_KEY, &phkResult);
			RegDeleteKeyExW(phkResult, L"iowin", KEY_WOW64_64KEY, 0);
			RegCloseKey(phkResult);

			//  hiber_storport.sys load/unload 64 times
			if (!ZwLoadDriver){
				printf("MMUnloadeddrvs list not cleared! \n");
				return true;
			}
			NTSTATUS ntStatus2;
			for (USHORT i = 0; i < 63; i++)
			{
				ntStatus2 = ZwLoadDriver(&fakeDriverName);
				//if (FAILED(ntStatus2))
				//	printf("[LoadDriver]ERROR NTSTATUS: %x \n", ntStatus);
				ZwUnloadDriver(&fakeDriverName);

			}
			ntStatus2 = ZwLoadDriver(&fakeDriverNameOnce);
			//if (FAILED(ntStatus2))
			//	printf("[LoadDriver]ERROR NTSTATUS: %x \n", ntStatus);
			ZwUnloadDriver(&fakeDriverNameOnce);
			return 0;
		}
		*(FARPROC *)&ZwLoadDriver = GetProcAddress(hDll, "ZwLoadDriver");

		if (!ZwLoadDriver)
			return true;


		if (!EnablePrivilege(L"SeLoadDriverPrivilege")) {
			printf("Privilages not obtained! \n");
			return true;
		}
		if (!CreateRegKey()) {
			printf("Registry failed! \n");
			return true;
		}

		printf("[LD]: %wZ \n", &usDriverName);
		NTSTATUS ntStatus = ZwLoadDriver(&usDriverName);
		if (FAILED(ntStatus))
			printf("ERROR LOADING: %x \n", ntStatus);
		else {
			printf("WinIo loaded successfully!\n");
			return 0;
		}
	}

	
private:

	HINSTANCE hDll = 0;
	UNICODE_STRING usDriverName;
	UNICODE_STRING fakeDriverName;
	UNICODE_STRING fakeDriverNameOnce;

	NTSTATUS(NTAPI *ZwLoadDriver)(IN PUNICODE_STRING DriverServiceName);
	NTSTATUS(NTAPI *ZwUnloadDriver)(IN PUNICODE_STRING DriverServiceName);

	LPWSTR lpSubKey = L"System\\CurrentControlSet\\Services\\iowin";
	HKEY hKey = 0;
	HKEY phkResult;
	/*
	* Change process tokens to allow NtLoadDriver
	*/
	bool EnablePrivilege(LPCWSTR lpPrivilegeName)
	{
		TOKEN_PRIVILEGES Privilege;
		HANDLE hToken;
		DWORD dwErrorCode;

		Privilege.PrivilegeCount = 1;
		Privilege.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
		if (!LookupPrivilegeValueW(NULL, lpPrivilegeName,
			&Privilege.Privileges[0].Luid))
			return GetLastError();

		if (!OpenProcessToken(GetCurrentProcess(),
			TOKEN_ADJUST_PRIVILEGES, &hToken))
			return GetLastError();

		if (!AdjustTokenPrivileges(hToken, FALSE, &Privilege, sizeof(Privilege),
			NULL, NULL)) {
			dwErrorCode = GetLastError();
			CloseHandle(hToken);
			return dwErrorCode;
		}

		CloseHandle(hToken);
		return TRUE;
	}
	/*
	* Manually create service reg key to control it with via SC.exe (ServiceControl)               
	*/
	bool CreateRegKey()
	{
		// RegSetKey Values
		ULONG dwErrorCode;
		DWORD Reserved = 0;
		LPTSTR lpClass = NULL;
		DWORD dwOptions = 0;
		REGSAM samDesired = KEY_ALL_ACCESS;
		LPSECURITY_ATTRIBUTES lpSecurityAttributes = NULL;
		// Set Value settings
		DWORD dwDisposition;
		DWORD dwServiceType = 1;
		DWORD dwServiceErrorControl = 1;
		DWORD dwServiceStart = 3;
		// RegSetValueExW values for image
		LPCTSTR lpValueImagePath = L"ImagePath";
		DWORD dwType = REG_EXPAND_SZ;
		LPWSTR ServiceImagePath = L"WinIo64.sys";
		SIZE_T ServiceImagePathSize;
		// RegSetValueExW values for Type
		LPCTSTR lpValueImageType = L"Type";
		DWORD dwTypeDWord = REG_DWORD;
		// RegSetValueExW values for Error
		LPCTSTR lpValueError = L"ErrorControl";
		// RegSetValueExW values for Start
		LPCTSTR lpValueStart = L"Start";

		// Main key creation
		dwErrorCode = RegCreateKeyExW(HKEY_LOCAL_MACHINE,
			lpSubKey,
			Reserved,
			lpClass,
			dwOptions,
			samDesired,
			lpSecurityAttributes,
			&phkResult,
			&dwDisposition);
		if (dwErrorCode) {
			RegCloseKey(phkResult);
			return FALSE;
		}
		// Set ImagePath
		ServiceImagePathSize = (lstrlenW(ServiceImagePath) + 1) * sizeof(WCHAR);
		dwErrorCode = RegSetValueExW(phkResult,
			lpValueImagePath,
			Reserved,
			dwType,
			(const BYTE *)ServiceImagePath,
			ServiceImagePathSize);
		if (dwErrorCode) {
			RegCloseKey(phkResult);
			return FALSE;
		}
		// Set the Type for key
		dwErrorCode = RegSetValueExW(phkResult,
			lpValueImageType,
			Reserved,
			dwTypeDWord,
			(const BYTE *)&dwServiceType,
			sizeof(DWORD));
		if (dwErrorCode) {
			RegCloseKey(phkResult);
			return FALSE;
		}
		// Set the ErrorControl key
		dwErrorCode = RegSetValueExW(phkResult,
			lpValueError,
			Reserved,
			dwTypeDWord,
			(const BYTE *)&dwServiceErrorControl,
			sizeof(DWORD));
		if (dwErrorCode) {
			RegCloseKey(phkResult);
			return FALSE;
		}
		// Set the Start key
		dwErrorCode = RegSetValueExW(phkResult,
			lpValueStart,
			Reserved,
			dwTypeDWord,
			(const BYTE *)&dwServiceStart,
			sizeof(DWORD));
		if (dwErrorCode) {
			RegCloseKey(phkResult);
			return FALSE;
		}
		RegCloseKey(phkResult);
		return TRUE;
	}

};