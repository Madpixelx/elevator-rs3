#pragma once
#include "Main.h"
#include "Superfetch.h"
#include "WINIO.h"
#include "StuffManager.h"
#include <string>
#define DELETECODE 123


class MemManipulator {
public:
	MemManipulator() {

	};
	~MemManipulator() {

	};

	HANDLE hDriver = 0; // narazie public
	NTSTATUS status = 0; // uzywany do sprawdzenia czy konstruktor da� rade
	uint8_t *ramImage = 0;
	int nOfRange = 0;
	BYTE driverWasRunning = 0;

	BYTE SetupMM();
	uint64_t cacheObjHdr(uint64_t addr = 0);
	//uint64_t cacheObjHdr(uint64_t addr, std::wstring fileName);
	uint64_t cacheMemRanges(SFMemoryInfo data[], int nOfRanges, std::wstring fileName);
	VOID repatch(BYTE type);
	bool isPoolPage(uint64_t addr, PfnList* pfnList);
	static BOOLEAN MapPhysicalMemory(HANDLE PhysicalMemory, PDWORD64 Address, PSIZE_T Length, PDWORD64 VirtualAddress);
	static BOOLEAN UnMapmemory(PDWORD64 Address);
	//static HANDLE OpenPhysicalMemory();
	int isAscii(int c);
	int isPrintable(uint32_t uint32);
	bool isInsidePhysicalRAM(uint64_t addr, SFMemoryInfo* mi, int nOfRange);
	uint64_t GetProcessBase(int pid);
	uint64_t GetDirBase(int pid);

	uint64_t pObjHdr = 0;
	PfnList* pfnTable = 0;
	uint16_t patched = 0;
	OBJECT_ATTRIBUTES attributes;

	UNICODE_STRING			physmemString;
	const WCHAR				physmemName[23] = L"\\device\\physicalmemory";
private:
	
	SFMemoryInfo memRanges[32] = { 0 };
	
	//Windows 10 RS3 16299.15 offsets
	uint64_t EPNameOffset = 0x450;
	uint64_t EPPidOffset = 0x02E0;
	uint64_t EPDirBaseOffset = 0x0028;
	uint64_t EPBaseOffset = 0x03C0;
	uint64_t EPLinkOffset = 0x02E8;
	uint64_t mKernelDir = 0;

	uint64_t TranslateLinearAddress(uint64_t directoryTableBase, uint64_t virtualAddress);
	BYTE Read(uint64_t address, uint8_t* buffer, int size);
	BYTE Write(uint64_t address, uint8_t* buffer, int size);
	BYTE ReadVirtual(uint64_t dirbase, uint64_t address, uint8_t* buffer, int size);
	BYTE WriteVirtual(uint64_t dirbase, uint64_t address, uint8_t* buffer, int size);
	uint64_t GetKernelDirBase();
	uint64_t GetEProcess(int pid);
	BYTE ScanPoolTag(char* tag_char, std::function<BYTE(uint64_t)> scan_callback);
	BYTE isInRam(uint64_t address, uint32_t len);
};


class DaclManipulator :MemManipulator {
public:
	bool restoreDacl;
	DaclManipulator() {
		restoreDacl = 0;
		RtlInitUnicodeString(&physmemString, physmemName);
	}

	BYTE ChangeSDPhysicalMemory() {
		InitializeObjectAttributes(&attributes, &physmemString, OBJ_CASE_INSENSITIVE, NULL, NULL);
		if (!restoreDacl)
		{			
			NTSTATUS status = ZwOpenSection(&physmem, WRITE_DAC | READ_CONTROL, &attributes);
			if (status == 0) {
				GetSecurityInfo(physmem, SE_KERNEL_OBJECT, DACL_SECURITY_INFORMATION, NULL, NULL, &OldDacl, NULL, 0);
				//BYTE  AclRevision;
				//BYTE  Sbz1;
				//WORD   AclSize;
				//WORD   AceCount;
				//WORD   Sbz2;

				Access.grfAccessPermissions = SECTION_ALL_ACCESS;
				Access.grfAccessMode = GRANT_ACCESS;
				Access.grfInheritance = NO_INHERITANCE;
				Access.Trustee.MultipleTrusteeOperation = NO_MULTIPLE_TRUSTEE;
				Access.Trustee.TrusteeForm = TRUSTEE_IS_NAME;
				Access.Trustee.TrusteeType = TRUSTEE_IS_USER;
				Access.Trustee.ptstrName = L"CURRENT_USER";
				SetEntriesInAclW(1, &Access, OldDacl, &NewDacl);
				SetSecurityInfo(physmem, SE_KERNEL_OBJECT, DACL_SECURITY_INFORMATION, NULL, NULL, NewDacl, NULL);
				LocalFree(NewDacl);
				// przywrocenie dacl
				CloseHandle(physmem);
				restoreDacl = 1;
				return true;
			}
			else
				printf("[ERROR] ZwOpenSection failed (first)");
		}
		else
		{
			printf("[DEB #4] Repatching DACL... \n");
			NTSTATUS	status = ZwOpenSection(&physmem, WRITE_DAC | READ_CONTROL, &attributes);
			if (status == 0) //STATUS_SUCCESS
			{
				SetSecurityInfo(physmem, SE_KERNEL_OBJECT, DACL_SECURITY_INFORMATION, NULL, NULL, OldDacl, NULL);
				CloseHandle(physmem);
				restoreDacl = 0;
				return true;
			}
			else {
				printf("[ERROR] Repatch failed! %llx\n", status);
				if (status == 0xC0000022) // STATUS_ACCESS_DENIED
					printf("[ERROR] ^ STATUS_ACCESS_DENIED, repatch probably failed\n");;
				return false;
			}
		}
	}



private:
	SECURITY_DESCRIPTOR sd = {};
	PSECURITY_DESCRIPTOR psd = &sd;

	HANDLE		physmem = NULL;
	PACL		OldDacl = NULL,
		NewDacl = NULL;

	EXPLICIT_ACCESS	Access;
};
